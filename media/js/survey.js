var valid_dp_31;
var valid_dp_32;
var valid_dp_33;

$(function () {
	function find_container(input) {
		return input.parent().parent();
	}
	function remove_validation_markup(input) {
		var cont = find_container(input);
		cont.removeClass('error success warning');
	}
	function add_validation_markup(input, cls) {
		var cont = find_container(input);
		cont.addClass(cls);
		input.addClass(cls);
	}
	function remove_all_validation_markup(form) {
		$('.error, .success, .warning', form).removeClass('error success warning');
	}
	$('form').each(function () {
		var form = $(this);
		form.validator({"errorClass":"error"})
		.bind('reset.validator', function () {
			remove_all_validation_markup(form);
		})
		.bind('onSuccess', function (e, ok) {
			$.each(ok, function() {
				var input = $(this);
				remove_validation_markup(input);
			}); 
		})
		.bind('onFail', function (e, errors) {
			$.each(errors, function() {
				var err = this;
				var input = $(err.input);
				remove_validation_markup(input);
				add_validation_markup(input, 'error');
			});
			return false;
		});
	});

	function clean_datatimepicker_controller(input){
		controller_group = input.parent().parent().parent();
		controller_group.removeClass('error success warning');
		return controller_group;
	}

	function validate_datetimepicker(input){
		if(input.data("validator").checkValidity()){
			clean_datatimepicker_controller(input).addClass("success");
			return true;
		}
		clean_datatimepicker_controller(input).addClass("error");
		return false;
	}

	$('#survey').submit(function () {
		var form      = $(this);
		var all_valid = true;

		vd_dtp_31 = validate_datetimepicker(dtp_31); 
		vd_dtp_32 = validate_datetimepicker(dtp_32);
		vd_dtp_33 = validate_datetimepicker(dtp_33);

		var valid_dates = vd_dtp_31 && vd_dtp_32 && vd_dtp_33; 

		if (!valid_dates) {
			form.data('validator').invalidate({});
			return false;
		}
	});

	$("#magasin_geant_visite").click(function(){
		if($(this).is(":checked")){
			$("#visite_magasin_geant")[0].disabled = false;
		}else{
			$("#visite_magasin_geant")[0].disabled = true;
		}
	});

	$("#accompagne").click(function() {
		if($(this).is(":checked")){
			$("#nbre_personnes" )[0].disabled = false;
			$("#accompagne_type")[0].disabled = false;
		}else{
			$("#nbre_personnes" )[0].disabled = true;
			$("#accompagne_type")[0].disabled = true;
	}});

	
	
});

$(document).ready(function() {
	$('#datetimepicker31').datetimepicker({pickTime: false});
	$('#datetimepicker32').datetimepicker({pickDate: false});
	$('#datetimepicker33').datetimepicker({pickDate: false});

	dtp_31 = $('#datetimepicker31 :input').validator();
	dtp_32 = $('#datetimepicker32 :input').validator();
	dtp_33 = $('#datetimepicker33 :input').validator();

	$.tools.validator.fn("select", function(el, value) {
		return value != "0" ? true : "selection required";
	});
});