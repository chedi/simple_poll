# coding=utf8
# -*- coding: utf8 -*-

from poll.models      import *
from django.db.models import Q
from prettyprint      import pp

hb_polls    = Survey.objects.filter(Q(magasin_geant_visite=False) & Q(boutique_gc=False) & Q(fast_food_gc=False))
geant_polls = Survey.objects.filter(Q(magasin_geant_visite=True ) | Q(boutique_gc=True ) | Q(fast_food_gc=True ))

def print_array(ar):
	for row in ar:
		print ("%d : %s" % (row[0], row[1]))

def print_multi_array(ar):
	col_data = "data :"
	for col in ar[0][0]:
		col_data += col[-1] + ":"
	print(col_data[:-1])
	for row in ar:
		row_data = row[-1] + ":"
		for col in row[:-1]:
			for inter in col:
				row_data += str(inter[0]) + ":"
		print(row_data[:-1])

def analyse(data , geant):
	rep_age = [
		[0, "15-24"     ],#1
		[0, "25-34"     ],#2
		[0, "35-49"     ],#3
		[0, "50 et plus"],#4
	]

	rep_sex = [
		[0, "Homme"],#1
		[0, "Femme"],#2
	]

	rep_activite = [
		[0, "Au Foyer"],#1
		[0, "Actif"   ],#2
		[0, "Etudiant"],#3
		[0, "Autre"   ],#4
	]

	rep_niveau = [
		[0, "Primaire"     ],#1
		[0, "Secondaire"   ],#2
		[0, "Universitaire"],#3
	]

	rep_quartier = [
		[0, "1 - Tunis centre ville"                     ],#1
		[0, "2 - Marsa, carthage, Sidi Bousaid, Gammarth"],#2
		[0, "3 - Kram, La goulette"                      ],#3
		[0, "4 - Mutuelle ville, Menzah 1, Cité jardin"  ],#4
		[0, "5 - El oumrane, Bardo"                      ],#5
		[0, "6 - Manar, Menzah 5..9"                     ],#6
		[0, "7 - Ariana, Melaha, Ennozha"                ],#7
		[0, "8 - Enasr, Riadh el andalous"               ],#8
		[0, "9 - El ghazala, Borj el ouzir"              ],#9
		[0, "10 - Soukra, ain zaghouen"                  ],#10
		[0, "11 - Lac 1/2"                               ],#11
		[0, "12 - Manouba, oued el lil, Ksar said"       ],#12
		[0, "13 - Jardin el menzh"                       ],#13
		[0, "14 - Intilaka, Ettadhamon, Mnihla"          ],#14
		[0, "15 - Lakanya, Wardia, Jbel el jloud"        ],#15
		[0, "16 - Megrine, Rades, Ezzahra"               ],#16
		[0, "17 - Hammam lif, Borj essedria, Boumhal"    ],#17
		[0, "18 - Montfleury, Rabta, Bab saïdoun"        ],#18
		[0, "19 - Sijoumi, Ezzouhour, El Akba, Dandan"   ],#19
		[0, "20 - Ben Arous, Madina el jadida"           ],#20
		[0, "21 - Mourouj, Ibn sina"                     ],#21
		[0, "22 - Sousse"                                ],#22
		[0, "23 - Sfax"                                  ],#23
		[0, "24 - Bizerte"                               ],#24
		[0, "25 - Nabeul"                                ],#25
		[0, "26 - Beja"                                  ],#26
		[0, "27 - zaghouen"                              ],#27
		[0, "28 - Autre"                                 ],#28
	]

	rep_residence = [
		[0, "1 - Tunis centre ville"                     ],#1
		[0, "2 - Marsa, carthage, Sidi Bousaid, Gammarth"],#2
		[0, "3 - Kram, La goulette"                      ],#3
		[0, "4 - Mutuelle ville, Menzah 1, Cité jardin"  ],#4
		[0, "5 - El oumrane, Bardo"                      ],#5
		[0, "6 - Manar, Menzah 5..9"                     ],#6
		[0, "7 - Ariana, Melaha, Ennozha"                ],#7
		[0, "8 - Enasr, Riadh el andalous"               ],#8
		[0, "9 - El ghazala, Borj el ouzir"              ],#9
		[0, "10 - Soukra, ain zaghouen"                  ],#10
		[0, "11 - Lac 1/2"                               ],#11
		[0, "12 - Manouba, oued el lil, Ksar said"       ],#12
		[0, "13 - Jardin el menzh"                       ],#13
		[0, "14 - Intilaka, Ettadhamon, Mnihla"          ],#14
		[0, "15 - Lakanya, Wardia, Jbel el jloud"        ],#15
		[0, "16 - Megrine, Rades, Ezzahra"               ],#16
		[0, "17 - Hammam lif, Borj essedria, Boumhal"    ],#17
		[0, "18 - Montfleury, Rabta, Bab saïdoun"        ],#18
		[0, "19 - Sijoumi, Ezzouhour, El Akba, Dandan"   ],#19
		[0, "20 - Ben Arous, Madina el jadida"           ],#20
		[0, "21 - Mourouj, Ibn sina"                     ],#21
		[0, "22 - Sousse"                                ],#22
		[0, "23 - Sfax"                                  ],#23
		[0, "24 - Bizerte"                               ],#24
		[0, "25 - Nabeul"                                ],#25
		[0, "26 - Beja"                                  ],#26
		[0, "27 - zaghouen"                              ],#27
		[0, "28 - Autre"                                 ],#28
	]

	frequence_geant = [
		[0, "1 fois / semaine"                             ], #1
		[0, "1 fois / mois"                                ], #2
		[0, "Plus d'une fois / semaine"                    ], #3
		[0, "Plus d'une fois / mois"                       ], #4
		[0, "Que pour un achat particulier - solde - promo"], #5
	]

	frequence_zara = [
		[0, "1 fois / semaine"                             ], #1
		[0, "1 fois / mois"                                ], #2
		[0, "Plus d'une fois / semaine"                    ], #3
		[0, "Plus d'une fois / mois"                       ], #4
		[0, "Que pour un achat particulier - solde - promo"], #5
	]
	nbr_individus       = 0
	la_grand_tunis      = 0
	lr_grand_tunis      = 0
	la_hors_grand_tunis = 0
	lr_hors_grand_tunis = 0
	la_different_lr     = 0

	for i in data:
		rep_sex        [i.sexe                 - 1][0] += 1
		rep_age        [i.age                  - 1][0] += 1
		rep_niveau     [i.niveau               - 1][0] += 1
		rep_activite   [i.activite             - 1][0] += 1
		rep_quartier   [int(i.quartier )       - 1][0] += 1
		rep_residence  [int(i.residence)       - 1][0] += 1
		if geant:
			frequence_geant[i.visite_magasin_geant - 1][0] += 1
		frequence_zara [i.visite_zara          - 1][0] += 1
		if i.nbre_personnes != None:
			nbr_individus += i.nbre_personnes

	for i in range(28):
		if i < 22:
			lr_grand_tunis += rep_quartier [i][0]
			la_grand_tunis += rep_residence[i][0]
		else:
			lr_hors_grand_tunis += rep_quartier [i][0]
			la_hors_grand_tunis += rep_residence[i][0]

	for i in geant_polls:
		if int(i.quartier) != int(i.residence):
			la_different_lr += 1

	print("Nombre de personne interogées   : %d"% (data.count()       ,))
	print("Nombre d'accompagants           : %d"% (nbr_individus      ,))
	print("lieu d'arrivé grand Tunis       : %d"% (la_grand_tunis     ,))
	print("lieu residence grand Tunis      : %d"% (lr_grand_tunis     ,))
	print("lieu d'arrivé hors grand Tunis  : %d"% (la_hors_grand_tunis,))
	print("lieu residence hors grand Tunis : %d"% (lr_hors_grand_tunis,))
	print("lieu arrivé <> residence        : %d"% (la_different_lr    ,))
	print("age") 
	print_array(rep_age        )
	print("sex") 
	print_array(rep_sex        )
	print("activité") 
	print_array(rep_activite   )
	print("niveau") 
	print_array(rep_niveau     )
	print("lieu arrivé") 
	print_array(rep_quartier   )
	print("lieu residence") 
	print_array(rep_residence  )
	print("Frequence geant") 
	print_array(frequence_geant)
	print("Frequence zara") 
	print_array(frequence_zara )

	geant_sexe_frequence = [
		[[
		[0, "1 fois / semaine"                             ], #1
		[0, "1 fois / mois"                                ], #2
		[0, "Plus d'une fois / semaine"                    ], #3
		[0, "Plus d'une fois / mois"                       ], #4
		[0, "Que pour un achat particulier - solde - promo"], #5
	], "Homme"],#1
		[[
		[0, "1 fois / semaine"                             ], #1
		[0, "1 fois / mois"                                ], #2
		[0, "Plus d'une fois / semaine"                    ], #3
		[0, "Plus d'une fois / mois"                       ], #4
		[0, "Que pour un achat particulier - solde - promo"], #5
	], "Femme"],#2
	]

	zara_sexe_frequence = [
		[[
		[0, "1 fois / semaine"                             ], #1
		[0, "1 fois / mois"                                ], #2
		[0, "Plus d'une fois / semaine"                    ], #3
		[0, "Plus d'une fois / mois"                       ], #4
		[0, "Que pour un achat particulier - solde - promo"], #5
	], "Homme"],#1
		[[
		[0, "1 fois / semaine"                             ], #1
		[0, "1 fois / mois"                                ], #2
		[0, "Plus d'une fois / semaine"                    ], #3
		[0, "Plus d'une fois / mois"                       ], #4
		[0, "Que pour un achat particulier - solde - promo"], #5
	], "Femme"],#2
	]

	geant_age_frequene = [
		[[
		[0, "1 fois / semaine"                             ], #1
		[0, "1 fois / mois"                                ], #2
		[0, "Plus d'une fois / semaine"                    ], #3
		[0, "Plus d'une fois / mois"                       ], #4
		[0, "Que pour un achat particulier - solde - promo"], #5
	], "15-24"     ],#1
		[[
		[0, "1 fois / semaine"                             ], #1
		[0, "1 fois / mois"                                ], #2
		[0, "Plus d'une fois / semaine"                    ], #3
		[0, "Plus d'une fois / mois"                       ], #4
		[0, "Que pour un achat particulier - solde - promo"], #5
	], "25-34"     ],#2
		[[
		[0, "1 fois / semaine"                             ], #1
		[0, "1 fois / mois"                                ], #2
		[0, "Plus d'une fois / semaine"                    ], #3
		[0, "Plus d'une fois / mois"                       ], #4
		[0, "Que pour un achat particulier - solde - promo"], #5
	], "35-49"     ],#3
		[[
		[0, "1 fois / semaine"                             ], #1
		[0, "1 fois / mois"                                ], #2
		[0, "Plus d'une fois / semaine"                    ], #3
		[0, "Plus d'une fois / mois"                       ], #4
		[0, "Que pour un achat particulier - solde - promo"], #5
	], "50 et plus"],#4
	]

	zara_age_frequene = [
		[[
		[0, "1 fois / semaine"                             ], #1
		[0, "1 fois / mois"                                ], #2
		[0, "Plus d'une fois / semaine"                    ], #3
		[0, "Plus d'une fois / mois"                       ], #4
		[0, "Que pour un achat particulier - solde - promo"], #5
	], "15-24"     ],#1
		[[
		[0, "1 fois / semaine"                             ], #1
		[0, "1 fois / mois"                                ], #2
		[0, "Plus d'une fois / semaine"                    ], #3
		[0, "Plus d'une fois / mois"                       ], #4
		[0, "Que pour un achat particulier - solde - promo"], #5
	], "25-34"     ],#2
		[[
		[0, "1 fois / semaine"                             ], #1
		[0, "1 fois / mois"                                ], #2
		[0, "Plus d'une fois / semaine"                    ], #3
		[0, "Plus d'une fois / mois"                       ], #4
		[0, "Que pour un achat particulier - solde - promo"], #5
	], "35-49"     ],#3
		[[
		[0, "1 fois / semaine"                             ], #1
		[0, "1 fois / mois"                                ], #2
		[0, "Plus d'une fois / semaine"                    ], #3
		[0, "Plus d'une fois / mois"                       ], #4
		[0, "Que pour un achat particulier - solde - promo"], #5
	], "50 et plus"],#4
	]

	for i in data:
		if geant:
			geant_sexe_frequence[i.sexe -1][0][i.visite_magasin_geant - 1][0] += 1
			geant_age_frequene  [i.age - 1][0][i.visite_magasin_geant - 1][0] += 1
		zara_sexe_frequence [i.sexe -1][0][i.visite_zara          - 1][0] += 1
		zara_age_frequene   [i.age - 1][0][i.visite_zara          - 1][0] += 1

	if geant:
		print("Fréquence geant / sexe")
		print_multi_array(geant_sexe_frequence)
		print("Fréquence geant / age")
		print_multi_array(geant_age_frequene)
	print("Fréquence zara / sexe")
	print_multi_array(zara_sexe_frequence)
	print("Fréqence zara / age")
	print_multi_array(zara_age_frequene)


def total():
	data = Survey.objects.all()
	rep_quartier = [
		[0, "1 - Tunis centre ville"                     ],#1
		[0, "2 - Marsa, carthage, Sidi Bousaid, Gammarth"],#2
		[0, "3 - Kram, La goulette"                      ],#3
		[0, "4 - Mutuelle ville, Menzah 1, Cité jardin"  ],#4
		[0, "5 - El oumrane, Bardo"                      ],#5
		[0, "6 - Manar, Menzah 5..9"                     ],#6
		[0, "7 - Ariana, Melaha, Ennozha"                ],#7
		[0, "8 - Enasr, Riadh el andalous"               ],#8
		[0, "9 - El ghazala, Borj el ouzir"              ],#9
		[0, "10 - Soukra, ain zaghouen"                  ],#10
		[0, "11 - Lac 1/2"                               ],#11
		[0, "12 - Manouba, oued el lil, Ksar said"       ],#12
		[0, "13 - Jardin el menzh"                       ],#13
		[0, "14 - Intilaka, Ettadhamon, Mnihla"          ],#14
		[0, "15 - Lakanya, Wardia, Jbel el jloud"        ],#15
		[0, "16 - Megrine, Rades, Ezzahra"               ],#16
		[0, "17 - Hammam lif, Borj essedria, Boumhal"    ],#17
		[0, "18 - Montfleury, Rabta, Bab saïdoun"        ],#18
		[0, "19 - Sijoumi, Ezzouhour, El Akba, Dandan"   ],#19
		[0, "20 - Ben Arous, Madina el jadida"           ],#20
		[0, "21 - Mourouj, Ibn sina"                     ],#21
		[0, "22 - Sousse"                                ],#22
		[0, "23 - Sfax"                                  ],#23
		[0, "24 - Bizerte"                               ],#24
		[0, "25 - Nabeul"                                ],#25
		[0, "26 - Beja"                                  ],#26
		[0, "27 - zaghouen"                              ],#27
		[0, "28 - Autre"                                 ],#28
	]

	rep_residence = [
		[0, "1 - Tunis centre ville"                     ],#1
		[0, "2 - Marsa, carthage, Sidi Bousaid, Gammarth"],#2
		[0, "3 - Kram, La goulette"                      ],#3
		[0, "4 - Mutuelle ville, Menzah 1, Cité jardin"  ],#4
		[0, "5 - El oumrane, Bardo"                      ],#5
		[0, "6 - Manar, Menzah 5..9"                     ],#6
		[0, "7 - Ariana, Melaha, Ennozha"                ],#7
		[0, "8 - Enasr, Riadh el andalous"               ],#8
		[0, "9 - El ghazala, Borj el ouzir"              ],#9
		[0, "10 - Soukra, ain zaghouen"                  ],#10
		[0, "11 - Lac 1/2"                               ],#11
		[0, "12 - Manouba, oued el lil, Ksar said"       ],#12
		[0, "13 - Jardin el menzh"                       ],#13
		[0, "14 - Intilaka, Ettadhamon, Mnihla"          ],#14
		[0, "15 - Lakanya, Wardia, Jbel el jloud"        ],#15
		[0, "16 - Megrine, Rades, Ezzahra"               ],#16
		[0, "17 - Hammam lif, Borj essedria, Boumhal"    ],#17
		[0, "18 - Montfleury, Rabta, Bab saïdoun"        ],#18
		[0, "19 - Sijoumi, Ezzouhour, El Akba, Dandan"   ],#19
		[0, "20 - Ben Arous, Madina el jadida"           ],#20
		[0, "21 - Mourouj, Ibn sina"                     ],#21
		[0, "22 - Sousse"                                ],#22
		[0, "23 - Sfax"                                  ],#23
		[0, "24 - Bizerte"                               ],#24
		[0, "25 - Nabeul"                                ],#25
		[0, "26 - Beja"                                  ],#26
		[0, "27 - zaghouen"                              ],#27
		[0, "28 - Autre"                                 ],#28
	]

	frequence = [
		[0, "1 fois / semaine"                             ], #1
		[0, "1 fois / mois"                                ], #2
		[0, "Plus d'une fois / semaine"                    ], #3
		[0, "Plus d'une fois / mois"                       ], #4
		[0, "Que pour un achat particulier - solde - promo"], #5
	]

	possession_voiture  = 0
	la_grand_tunis      = 0
	lr_grand_tunis      = 0
	la_hors_grand_tunis = 0
	lr_hors_grand_tunis = 0

	for i in data:
		rep_quartier   [int(i.quartier ) - 1][0] += 1
		rep_residence  [int(i.residence) - 1][0] += 1
		frequence      [i.visite_zara    - 1][0] += 1
		if i.voiture != '0' and i.voiture != 'vide':
			possession_voiture += 1

	for i in range(28):
		if i < 22:
			lr_grand_tunis += rep_quartier [i][0]
			la_grand_tunis += rep_residence[i][0]
		else:
			lr_hors_grand_tunis += rep_quartier [i][0]
			la_hors_grand_tunis += rep_residence[i][0]

	print("lieu d'arrivé grand Tunis       : %d"% (la_grand_tunis     ,))
	print("lieu residence grand Tunis      : %d"% (lr_grand_tunis     ,))
	print("lieu d'arrivé hors grand Tunis  : %d"% (la_hors_grand_tunis,))
	print("lieu residence hors grand Tunis : %d"% (lr_hors_grand_tunis,))

	print ("lieu arrivé")
	print_array(rep_quartier)
	print ("lieu résidence")
	print_array(rep_residence)
	print ("Fréquence")
	print_array(frequence)
	print("possession voiture : %d" % (possession_voiture,))
