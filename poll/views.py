from form                     import survey
from annoying.decorators      import render_to
from django.shortcuts         import redirect
from django.template          import RequestContext
from django.shortcuts         import render_to_response

@render_to('home.html')
def home(request):return {}

@render_to('error.html')
def error(request):return {}

def add_survey(request):
	if request.POST:
		ajax = request.is_ajax()
		form = survey(request.POST)
		if form.is_valid():
			form.save()
			if ajax:
				return HttpResponse(json.dumps({"status":True}), mimetype="application/json")
			else:
				return render_to_response('home.html', {"insertion_status": True}, context_instance=RequestContext(request))
		else:
			if ajax:
				return HttpResponse(json.dumps({"status":False}), mimetype="application/json")
			else:
				return render_to_response('error.html', {"errors": form.errors}, context_instance=RequestContext(request))
	else:
		return redirect("error")