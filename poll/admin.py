from poll.models    import *
from django.contrib import admin

class SurveyAdmin(admin.ModelAdmin):
	ordering      = ('numero_quest',)
	search_fields = ('numero_quest',)
	list_filter   = ('enqueteur', 'date_survey', 'magasin_geant_visite',)
	fieldsets = (
		('Survey info', {
			'classes': ('collapse in',),
			'fields': ('time_survey', 'date_survey', 'enqueteur', 'superviseur', 'numero_quest')
		}),
		('Q1', {
			'classes': ('collapse in',),
			'fields' : ('magasin_geant_visite', 'boutique_gc', 'fast_food_gc')
		}),
		('Q2', {
			'classes': ('collapse in',),
			'fields' : ('time_arrive',)
		}),
		('Q3', {
			'classes': ('collapse in',),
			'fields' : ('comment_arrivez',)
		}),
		('Q4', {
			'classes': ('collapse in',),
			'fields' : ('lieu_arrivez', 'quartier',)
		}),
		('Q5', {
			'classes': ('collapse in',),
			'fields' : ('visite_magasin_geant','visite_zara')
		}),
		('Q6', {
			'classes': ('collapse in',),
			'fields' : ('accompagne_type', 'accompagne','nbre_personnes')
		}),
		('Signalitique', {
			'classes': ('collapse in',),
			'fields' : ('voiture','residence','profession','telephone','age','sexe','niveau','statut','activite',)
		}),
	)

admin.site.register(Survey, SurveyAdmin)