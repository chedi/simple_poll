<div class="row-fluid control-group">
	<div class="span1">
		<strong>Q2</strong>
	</div>
	<label class="control-label span3" for="datetimepicker33">
		<strong>A quelle heure êtes vous arrivez ?</strong>
	</label>
	<div class="controls span8">
		<div id="datetimepicker33" class="input-append">
			<input data-format="hh:mm:ss" type="text" required="required" name="time_arrive">
			<span class="add-on">
				<i data-time-icon="icon-time" data-date-icon="icon-calendar"/>
			</span>
		</div> 
	</div>
</div>